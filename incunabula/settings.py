"""
Settings are kept either in general_settings.py, if they're something
which will probably be the same everywhere, or in local_settings.py
if they're something which depends on the installation.

local_settings.py doesn't live in the repository; it's listed in
.gitignore. You should make one by copying example_settings.py
in this directory. Further instructions are at the head of that file.
"""
from .general_settings import *
from .local_settings import *
