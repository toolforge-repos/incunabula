"""
Copy this file to local_settings.py in the same directory.
Then change the key, and tweak the settings as you see fit.
"""
from .general_settings import *
from pathlib import Path
import os

SECRET_KEY = 'daft'
assert SECRET_KEY is not 'daft', 'get on and change the key'
DEBUG = False

ALLOWED_HOSTS = []

WSGI_APPLICATION = 'incunabula.wsgi.application'

# Database
# https://docs.djangoproject.com/en/5.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/5.0/topics/i18n/

LANGUAGE_CODE = 'en-gb'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/5.0/howto/static-files/

STATIC_URL = 'static/'
STATIC_ROOT = os.path.join(
    os.path.dirname(__file__), '..', 'static')
