from django.apps import AppConfig


class FifteenerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fifteener'
