let page_number_box = null;
let page_prev = null;
let page_next = null;
let max_page_number_text = null;

let options_page_number = 0;
let zoomer = null;

function update_page_controls(new_page_number) {

        page_number_box.value = new_page_number;
        options_page_number = new_page_number;

        max_page_number_text.textContent = '/'+page_list.length;

        page_prev.disabled = (new_page_number<2); // can be 0
        page_next.disabled = (new_page_number==page_list.length);
}

function page_go_prev() {
        if (options_page_number <= 0) {
                return;
        }

        options_page_number -= 1;
        load_page(options_page_number);
        update_page_controls(options_page_number);
}

function page_go_next() {
        if (options_page_number >= page_list.length) {
                return;
        }

        options_page_number += 1;
        load_page(options_page_number);
        update_page_controls(options_page_number);
}

function page_go_to(e) {
        let new_page_number = page_number_box.value;

        if (new_page_number < 0 ||
                new_page_number >= page_list.length) {
                return;
        }

        load_page(new_page_number);
        update_page_controls(new_page_number);
}

function option_zoom(how_much) {
    if (zoomer) {
            zoomer.smoothZoom(0, 0, how_much);
    }
}

function option_zoom_in() {
        option_zoom(1.5);
}

function option_zoom_out() {
        option_zoom(0.5);
}

function options_setup() {
        let file = menu_new('File');

        menu_option_new(file, 'New', '', option_file_new);
        menu_option_new(file, 'Open...', '', option_file_open);
        menu_option_new(file, 'Save', 'disabled', null);
        menu_option_add_separator(file);
        menu_option_new(file, 'Log in...', '', null);

        let edit = menu_new('Edit');
        menu_option_new(edit, 'Settings...', '', null);
        menu_option_add_separator(edit);
        menu_option_new(edit, 'Unbreak', '', unbreak);

        let view = menu_new('View');
        menu_option_new(view, 'Previous page', 'disabled', null);
        menu_option_new(view, 'Next page', 'disabled', null);

        page_number_box = document.createElement('input');
        page_number_box.setAttribute('id', 'page_number_box');
        page_number_box.value = 0;
        page_number_box.onchange = page_go_to;
        menu_add_widget(page_number_box);

        max_page_number_text = document.createElement('span');
        max_page_number_text.setAttribute('id', 'max_page_number_text');
        menu_add_widget(max_page_number_text);

        page_prev = document.createElement('button');
        page_prev.textContent = '<';
        page_prev.setAttribute('id', 'page_prev');
        page_prev.onclick = page_go_prev;
        menu_add_widget(page_prev);

        page_next = document.createElement('button');
        page_next.textContent = '>';
        page_next.setAttribute('id', 'page_next');
        page_next.onclick = page_go_next;
        menu_add_widget(page_next);

        if (panzoom!=undefined) {
                let source_image = document.querySelector('#page-scan');
                zoomer = panzoom(source_image);
                document.querySelector('#zoom-in' ).onclick = option_zoom_in;
                document.querySelector('#zoom-out').onclick = option_zoom_out;
        } else {
                console.log("panzoom library not loaded");
        }

        update_page_controls(0);
}

function option_file_new() {
        let scanned_text = document.querySelector('#scanned-text');
        if (scanned_text.value != original_scanned_text) {
                if (!confirm('Click OK to clear your text and '+
                        'start afresh. Click Cancel to cancel.')) {
                        return;
                }
        }
        scanned_text.value = '';
        layer_select('main');
}

function option_file_open() {
        layer_select('open');
}

function option_please_wait() {
        document.querySelector('#logo' ).classList.add('waiting');
}

function option_stop_waiting() {
        document.querySelector('#logo' ).classList.remove('waiting');
}
