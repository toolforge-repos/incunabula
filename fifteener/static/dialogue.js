const WIKISOURCE_URL_PREFIX = 'https://'+HOSTNAME+'/wiki/Index:';
const MINIMUM_PAGE_SCAN_HEIGHT = 100;

function dialogue_setup() {
        document.querySelector('#open-button').onclick = bookopen_clicked;

        document.querySelector('#show-hostname').textContent = HOSTNAME;
}

function bookopen_clicked() {
        let bookname = document.querySelector('#bookname').value;
        let url = null;

        if (bookname=='') {
                return
        } else if (bookname.indexOf(WIKISOURCE_URL_PREFIX)==0) {
                url = bookname;
        } else {
                url = WIKISOURCE_URL_PREFIX + bookname.replace(/ /g,'_');
        }

        document.querySelector('#open-url').textContent = url;

        let instructions = document.querySelector('#open-instructions');
        instructions.textContent = 'Fetching the list of pages...';

        function bookopen_success(json) {
                let pages = json['query']['proofreadpagesinindex'];

                if (pages.length==0) {
                        instructions.textContent = 'That book has no pages!';
                        return;
                }

                // in case other updates fail:
                instructions.textContent = 'Success!';

                page_list = pages;

                update_page_controls(1);
                load_page(1);

                layer_select('main');
        }

        function bookopen_failure(response) {
                console.log("Lookup failure for "+url+". Response follows:");
                console.log(response);
                if (response.status==404) {
                        instructions.innerHTML = "Couldn't find that book.";
                } else if (response.status==0) {
                        instructions.innerHTML = "Problem reaching "+HOSTNAME+". "+
                                "Check your network connection.";
                } else {
                        instructions.innerHTML = "Loading failed. " +
                                "See the JavaScript console for more details.";
                }
        }

        mediawiki_get_page_list(bookname, bookopen_success, bookopen_failure);
}

function pageopen_clicked() {
        let pagename = pagelist.value;

        function pageopen_success(json) {

                load_page(1);

                dialogue_close();

        }

        mediawiki_get_page_list(pagename, pageopen_success, pageopen_failure);
}

function load_page(number) {
        const page_name = page_list[number]['title'];

        function pagetext_success(json) {
                const page_details = json['query']['pages'][0];
                console.log(page_details);
                let page_text = '';

                if ('revisions' in page_details) {
                        page_text = (
                                page_details['revisions']
                                [0]['slots']['main']['content']
                        );

                        rescan();
                } else {
                        document.querySelector('#comparison').textContent = (
                                "This page is empty... so far."
                        );
                }

                let page_scan = document.querySelector('#page-scan');

                document.querySelector('#scanned-text').textContent = page_text;
                page_scan.setAttribute('src',
                        page_details['imagesforpage']['fullsize']);

                if (page_scan.clientHeight < MINIMUM_PAGE_SCAN_HEIGHT) {
                        let layer_height = document.querySelector('#right').clientHeight;

                        console.log(
                                layer_height - MINIMUM_PAGE_SCAN_HEIGHT
                        );
                        document.querySelector('#comparison').style.height = (
                                (layer_height - MINIMUM_PAGE_SCAN_HEIGHT)+'px'
                        );

                }
        }

        function pagetext_failure(response) {
                console.log("Lookup failure for page text. Response follows:");
                console.log(response);
                if (response.status==404) {
                        alert("Couldn't find that page.");
                } else if (response.status==0) {
                        alert(
                                "Problem reaching "+HOSTNAME+". "+
                                "Check your network connection."
                        );
                } else {
                        alert("Loading failed. " +
                                "See the JavaScript console for more details.");
                }
        }

        mediawiki_get_page_contents(page_name, pagetext_success, pagetext_failure);
}
