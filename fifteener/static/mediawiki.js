const LANGUAGE = 'la';
const HOSTNAME = LANGUAGE+'.wikisource.org';
const ENDPOINT = 'https://'+HOSTNAME+'/w/api.php';
const USER_AGENT = 'incunabula/0.1';

function encodeGetParams(p) {
        return Object.entries(p).map(
                kv => kv.map(encodeURIComponent).join("=")).join("&");
}

function mediawiki_lookup(params, handle_success, handle_failure) {

        option_please_wait();

        let req = new XMLHttpRequest();
        req.open("GET", ENDPOINT+'?'+encodeGetParams(params), true);

        req.setRequestHeader(
                'User-Agent',
                USER_AGENT,
                );
        req.onreadystatechange = function() {
                if (this.readyState != 4) {
                        return;
                }

                option_stop_waiting();

                if (this.status == 200) {
                        let json = JSON.parse(this.responseText);
                        handle_success(json);
                } else {
                        handle_failure(this);
                }
        };

        req.send();
}

function mediawiki_get_page_list(bookname, handle_success, handle_failure) {
        let params = {
            'action': 'query',
            'list': 'proofreadpagesinindex',
            'prppiititle': 'Index:' + bookname,
            'prppiiprop': 'ids|title',
            'format': 'json',
            'origin' : '*'
        };

        mediawiki_lookup(params, handle_success, handle_failure);
}

function mediawiki_get_page_contents(pagename, handle_success, handle_failure) {
        let params = {
                'action': 'query',
                'format': 'json',
                'origin' : '*',
                'formatversion': 2,
                'titles': pagename,

                'prop': 'imageforpage|revisions',
                'prppifpprop': 'filename|fullsize',

                'rvprop': 'ids|timestamp|flags|content',
                'rvslots': 'main',
        };

        mediawiki_lookup(params, handle_success, handle_failure);
}
