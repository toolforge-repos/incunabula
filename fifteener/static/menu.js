const MENU_SELECTED_CLASS = 'menu-selected';

function menu_setup() {
        document.querySelectorAll('header div').forEach(
                (t) => {
                        t.addEventListener('mousedown', menu_clicked);
                        t.addEventListener('mouseenter', menu_enter);
                });
}


function select_a_menu(which) {
        document.querySelectorAll('header div').forEach(
                (t) => {
                        if (t==which) {
                                t.classList.add(MENU_SELECTED_CLASS);
                        } else {
                                t.classList.remove(MENU_SELECTED_CLASS);
                        }
                });
}

function menu_is_open() {
        return (document.querySelector('.'+MENU_SELECTED_CLASS)!==null);
}

function menu_clicked(e) {
        let h2 = e.target;
        let div = h2.parentNode;

        if (div.classList.contains(MENU_SELECTED_CLASS)) {
                div.classList.remove(MENU_SELECTED_CLASS);
        } else {
                select_a_menu(div);
        }
}

function menu_enter(e) {
        if (!menu_is_open()) {
                return;
        }

        let h2 = e.target;
        let div = h2.parentNode;

        select_a_menu(div);
}

function menu_new(caption) {
        let div_tag = document.createElement('div');
        let h2_tag = document.createElement('h2');
        let ul_tag = document.createElement('ul');

        h2_tag.appendChild(
                document.createTextNode(caption)
        );
        div_tag.appendChild(h2_tag);
        div_tag.appendChild(ul_tag);

        document.querySelector('header').appendChild(div_tag);

        h2_tag.addEventListener('mousedown', menu_clicked);
        div_tag.addEventListener('mouseenter', menu_enter);

        return ul_tag;
}

function menu_add_widget(widget) {
        document.querySelector('header').appendChild(widget);
}

function menu_option_new(menu, caption, cssClass, handler) {
        let li_tag = document.createElement('li');
        let a_tag = document.createElement('a');

        li_tag.setAttribute('href', '#');
        li_tag.addEventListener('mouseup', () => {
                select_a_menu(null);
                handler();
        });
        if (cssClass) {
                li_tag.classList.add(cssClass);
        }

        li_tag.appendChild(a_tag);
        a_tag.appendChild(
                document.createTextNode(caption)
        );
        menu.appendChild(li_tag);
}

function menu_option_add_separator(menu) {
        let hr_tag = document.createElement('hr');
        menu.appendChild(hr_tag);
}
