function layer_setup() {
        layer_select('start');

        document.querySelector('#close-button').onclick = layer_close_click;
}

function layer_close_click() {
        layer_select('main');
}

function layer_select(name) {
        name = 'layer-'+name;
        document.querySelectorAll('.layer').forEach(function(n) {
                if (n.getAttribute('id')==name) {
                        if (n.classList.contains('container')) {
                                n.style.display = 'flex';
                        } else {
                                n.style.display = 'inherit';
                        }
                } else {
                        n.style.display = 'none';
                }
        });

        let close_button_display = 'block';

        if (name=='layer-main') {
            close_button_display = 'none';
        }

        document.querySelector('#close-button').style.display = close_button_display;
}
