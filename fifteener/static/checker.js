// looked_up is a microcosm, a subset of the lexicon on the server.
//
// The keys are strings, the words in lowercase. The values are
// one of:
//     - KNOWN ("k"): this is known to the server as a word
//     - UNKNOWN ("u"): the server does not know this as a word
//     - an integer: the word is being looked up on the server.
//         In this case, there's a div with the class 'lookup'
//         and id=f'lookup{value}'.
//
let looked_up = {};

const UNKNOWN = 'u';
const KNOWN = 'k';

const NON_ALPHABETICAL_CHARS =
        // this *must* end with "-"
        '!?.,<{+=:¬/-';
const WHITESPACE_CHARS = ' \t\n';
const IGNORE_PREFIXES = '{< \t\n';

var lookup_counter = 0;

var previous_contents = {
    'scanned-text': '',
    'good-text': '',
}
var previous_caret_positions = {
    'scanned-text': -1,
    'good-text': -1,
}

let dragging = false;
let app = document.querySelector('.app');
let left_side = document.querySelector('#left');
let right_side = document.querySelector('#right');
let good_text = document.querySelector('.good-text');
let comparison = document.querySelector('.comparison');
let page_title = null;
let original_scanned_text = '';

let page_list = Array();

function lower_box_for_node(node) {
        while (true) {
                if (node==left_side) {
                        return good_text;
                } else if (node==right_side) {
                        return comparison;
                }

                node = node.parentNode;
        }
}
function fix_height(box, h) {
        box.style.height = h+"px";
        box.style.flexGrow = 0;
}

function all_ev(el, evs, cb) { for(let ev of evs) { el.addEventListener(ev,cb); } }
function selections(yn) { 
        document.querySelectorAll('.container').forEach(function(n) {
            n.style.userSelect = yn?'auto':'none';});
}

function drag(e) {
        if(!dragging) {
                dragging = true;
                let target = lower_box_for_node(e.target);
                fix_height(
                        target,
                        target.getBoundingClientRect().height
                );
        }
}

function undrag() {
        dragging = false;
        selections(true);
}

function move_drag(e) {
        if (dragging) {
                let target = lower_box_for_node(e.target);
                fix_height(target,
                        target.getBoundingClientRect().bottom-e.clientY);
        }
}

function setup() {
        all_ev(document.querySelector('#draggy-left'),['mousedown','touchstart'],drag);
        all_ev(document.querySelector('#draggy-right'),['mousedown','touchstart'],drag);
        all_ev(document.querySelector('.container'),['mouseup','mouseleave','touchend'],undrag);
        all_ev(document.querySelector('.container'),['mousemove','touchmove'],move_drag);

        all_ev(document.querySelector('.scanned-text'),
                ['keyup','mouseup'],handle_textarea_change);
        all_ev(good_text,['keyup','mouseup'],handle_textarea_change);

        menu_setup();
        dialogue_setup();
        options_setup();
        layer_setup();

        get_sigla();
        rescan();

        let app_size = (
                document.body.clientHeight -
                document.querySelector('header').getBoundingClientRect().bottom
        );

        fix_height(good_text, 100);
        fix_height(comparison, app_size);

}

function handle_textarea_change(e) {
        var textarea = e.target;
        var name = textarea.id;
        var current_contents = textarea.value;
        var caret_position = textarea.selectionStart;

        if (current_contents != previous_contents[name]) {
                rescan();

                previous_contents[name] = current_contents;

                previous_caret_positions[name] = -1;
        }

        if (caret_position != previous_caret_positions[name]) {
                handle_caret_move(textarea, caret_position);
        }
}

function handle_caret_move(textarea, caret_position) {

        var index_name = 'idx_'+textarea.id.substring(0,1);

        var previous = undefined;
        var previous_offset = 0;
        var found = false;
        var maybe_final = undefined;

        if (caret_position==undefined) {
                caret_position = textarea.selectionStart;
        }

        function remove_previous_current() {
                document.querySelectorAll('.current').forEach(function(n) {
                        n.classList.remove('current');
                });
        }

        document.querySelectorAll('.lexeme').forEach(function(lexeme) {

                let current_offset_attr = lexeme.attributes[index_name];

                if (current_offset_attr==undefined) {
                        return false;
                }

                maybe_final = lexeme;

                let current_offset = current_offset_attr.value * 1;

                if (previous_offset<=caret_position
                        && caret_position<current_offset) {

                        remove_previous_current()
                        if (previous!==undefined) {
                                previous.classList.add("current");
                                scrollIntoViewIfNotVisible(previous);
                        }
                        found = true;

                        return false;
                }

                previous_offset = current_offset;
                previous = lexeme;
        });

        if (!found) {
                remove_previous_current();
                if (maybe_final!=undefined) {
                        maybe_final.classList.add("current");
                        scrollIntoViewIfNotVisible(maybe_final);
                }
        }
}

function are_you_sure(e) {
        // "Are you sure you want to close this tab?"
        // This is only set as the handler when it's
        // needed; we don't need to check the flag again.
        e.preventDefault();
        e.returnValue = true;
}

function word_pattern() {
        return RegExp(
                '(<[^>]+>)|(\{\{[^}]*\}\})|[ \n\t]|('+
                '[^ \n\t' + NON_ALPHABETICAL_CHARS + ']+)'+
                '|(.)',
                'dgi'
        );
}

function rescan() {
        var scanned_text = document.querySelector('#scanned-text').value;
        var good_text = document.querySelector('#good-text').value;
        var comparison = document.querySelector('#comparison');

        comparison.innerHTML = '';

        let count = 0;
        let found;
        let munged;
        let noinclude = false;

        let re_s = word_pattern();

        while (found = re_s.exec(scanned_text)) {
                let word = found[0];
                let word0 = word.charAt(0);

                if (word=='') {
                        break;
                } else if (word=='<noinclude>') {
                        noinclude = true;
                        continue;
                } else if (word=='</noinclude>') {
                        noinclude = false;
                        continue;
                } else if (word0=='\n') {
                        let txt = document.createElement('span');
                        txt.classList.add('nonword');
                        txt.classList.add('pilcrow');
                        txt.appendChild(
                                document.createTextNode('¶'));
                        comparison.appendChild(txt);
                        continue;
                } else if (IGNORE_PREFIXES.indexOf(word0)!=-1 ||
                        noinclude) {
                        continue;
                } else if (NON_ALPHABETICAL_CHARS.indexOf(word0)!=-1) {
                        let txt = document.createElement('span');
                        txt.classList.add('nonword');
                        txt.appendChild(
                                document.createTextNode(word));
                        comparison.appendChild(txt);
                        continue;
                }

                let lexeme_tag = document.createElement('div');
                lexeme_tag.classList.add(
                        'lexeme',
                );
                lexeme_tag.setAttribute(
                        'id',
                        'i'+count,
                );
                lexeme_tag.setAttribute(
                        'idx_s',
                        re_s.lastIndex-word.length,
                );
                count += 1;

                let scan_tag = document.createElement('div');
                scan_tag.classList.add('scan');
                lexeme_tag.appendChild(scan_tag);
                scan_tag.appendChild(
                        document.createTextNode(word)
                );

                let good_tag = document.createElement('div');
                good_tag.classList.add('good');
                lexeme_tag.appendChild(good_tag);

                let lex_tag = document.createElement('div');
                lex_tag.classList.add('lex');
                lexeme_tag.appendChild(lex_tag);
                munge(word).forEach(function(w) {
                        a_tag = document.createElement('a');
                        a_tag.appendChild(
                                document.createTextNode(w)
                        );
                        lex_tag.appendChild(a_tag);
                });

                comparison.appendChild(lexeme_tag);
                comparison.appendChild(document.createTextNode('\n'));
        }

        let re_g = word_pattern();
        count = 0;

        while (found = re_g.exec(good_text)) {

                let word = found[0];
                let word0 = word.charAt(0);

                if (word=='') {
                        break;
                } else if (IGNORE_PREFIXES.indexOf(word0)!=-1) {
                        continue;
                } else if (NON_ALPHABETICAL_CHARS.indexOf(word0)!=-1) {
                        continue;
                }

                var lexeme_tag = document.querySelector('#i'+count);
                count += 1;

                if (!lexeme_tag) {
                        break;
                }
                var scanned_word_tag = lexeme_tag.querySelector('div.scan');

                lexeme_tag.setAttribute(
                        'idx_g',
                        re_g.lastIndex-word.length,
                );

                var good_tag = lexeme_tag.querySelector('div.good');
                good_tag.appendChild(
                        document.createTextNode(word)
                );

                let good_word = word.toLowerCase();

                matches = false;

                munge(scanned_word_tag.textContent).forEach(function(w) {
                        if (w==good_word) {
                                matches = true;
                        }
                });

                if (matches) {
                        lexeme_tag.classList.add('matches');
                } else {
                        lexeme_tag.classList.add('no-matches');
                }
        }

        document.querySelectorAll('.lexeme').forEach(function(lexeme_tag) {

                var scanned_word_tag = lexeme_tag.querySelector('div.scan');
                var good_word_tag = lexeme_tag.querySelector('div.good');
                var lex_tag = lexeme_tag.querySelector('div.lex');
                var munged = lex_tag.querySelectorAll('a');

                look_up(munged).forEach(function(w) {
                        lex_tag.appendChild(w);
                });

        });

        maybe_look_stuff_up();

        if (page_title==null) {
                document.title = 'Incunabula checker';
        } else if (scanned_text != original_scanned_text) {
                document.title = '* '+page_title;
                window.addEventListener('beforeunload', are_you_sure);
        } else {
                document.title = page_title;
                window.removeEventListener('beforeunload', are_you_sure);
        }
}

function set_lexeme_as_known_or_unknown(link, known) {
        if (known==KNOWN) {
                var word = link.textContent;
                link.setAttribute('href',
                        'https://en.wiktionary.org/wiki/' + word + '#Latin');
                link.setAttribute('target', 'wiki');
                link.classList.add(KNOWN);
        } else {
                link.classList.add(UNKNOWN);
        }
}

function look_up(munged) {

        result = [];

        munged.forEach(function(link) {
                var found = looked_up[link.textContent];

                if (found==KNOWN || found==UNKNOWN) {

                        set_lexeme_as_known_or_unknown(link, found);

                } else {
                        if (found==undefined) {
                                lookup_counter += 1;
                                looked_up[link.textContent] = lookup_counter;
                                found = lookup_counter;
                        }

                        link.classList.add('lookup');
                        link.setAttribute('id', 'lookup'+found);
                }
        });

        return munged;
}

function scrollIntoViewIfNotVisible(target) { 
        if (target.getBoundingClientRect().bottom > window.innerHeight) {
                target.scrollIntoView(false);
        }

        if (target.getBoundingClientRect().top < 0) {
                target.scrollIntoView();
        } 
}

function maybe_look_stuff_up() {
        var args = {};
        var count = 0;

        document.querySelectorAll('.lookup').forEach(function(n) {
                var key = n.getAttribute('id').substr(6);
                var value = n.textContent;
                args[key] = value;
                count += 1;
        });

        if (count==0) {
                return;
        }

        fetch('api/lookup', {
                'method': 'POST',
                'body': JSON.stringify(args),
                'headers': {
                        'Content-Type': 'application/json',
                },
        }).then(response => {
                if (!response.ok) {
                        throw new Error("it didn't work"); // FIXME
                }
                return response.json();
        }).then(data => {
                handle_lookup_success(data);
        });
}

function handle_lookup_success(data) {

        function record_findings(known) {
                data[known].forEach(function(w) {
                        var tags = document.querySelectorAll("#lookup"+w);

                        tags.forEach(function(tag) {
                                var word = tag.textContent;

                                tag.classList.remove('lookup');
                                set_lexeme_as_known_or_unknown(tag, known);

                                tag.parentNode.classList.remove('looking-up');

                                looked_up[word] = known;
                        });
                });
        }

        record_findings(KNOWN);
        record_findings(UNKNOWN);
}

function unbreak() {
        function unbreakage_pattern() {
                return RegExp(
                        "(([^" + WHITESPACE_CHARS+ NON_ALPHABETICAL_CHARS + "]+)" +
                        "¬?\n" +
                        "([^" + WHITESPACE_CHARS + NON_ALPHABETICAL_CHARS + "]+))",
                        'dgim'
                );
        }

        let unknown = {};

        function try_unbreaking(do_the_replace) {
                let scanned_text = document.querySelector('#scanned-text').value;

                scanned_text = scanned_text.replace(
                        unbreakage_pattern(),
                        function(match, full, last_word, first_word,
                                offset, string, groups) {


                                let combined_word = last_word + first_word;
                                let unknown_possibilities = [];
                                let result = undefined;

                                for (let w of munge(combined_word)) {
                                        let known = looked_up[w];

                                        if (do_the_replace) {
                                                if (known==KNOWN) {
                                                        result = "\n"+combined_word+"\n";
                                                        break;
                                                }
                                        } else {
                                                if (known==KNOWN) {
                                                        unknown_possibilities = [];
                                                        break;
                                                } else if (known!=UNKNOWN) {
                                                        // undefined, or integer
                                                        unknown_possibilities.push(w);
                                                }
                                        }

                                }

                                if (result!==undefined) {
                                        return result;
                                }

                                for (let u of unknown_possibilities) {
                                        let i = Object.keys(unknown).length;
                                        unknown[i] = u;
                                }

                                // fallback case: make no change
                                return full;
                         }
                );
                return scanned_text;
        }

        try_unbreaking(false);

        if (unknown!={}) {
                fetch('api/lookup', {
                        'method': 'POST',
                        'body': JSON.stringify(unknown),
                        'headers': {
                                'Content-Type': 'application/json',
                        },
                }).then(response => {
                        if (!response.ok) {
                                throw new Error("it didn't work"); // FIXME
                        }
                        return response.json();
                }).then(data => {
                        r = data;
                        // XXX refactor
                        data[KNOWN].forEach(function(x) {
                                looked_up[unknown[x]] = KNOWN;
                        });
                        data[UNKNOWN].forEach(function(x) {
                                looked_up[unknown[x]] = UNKNOWN;
                        });
                });
        }

        let fixed_text = try_unbreaking(true);

        document.querySelector('#scanned-text').value = fixed_text;
}
