const PLUS_NASAL = {
        'ā': 'a',
        'ē': 'e',
        'ī': 'i',
        'ō': 'o',
        'ū': 'u',
};

const DEFAULT_SIGLA = (
        'ſ s\n'+
        '⁊ et\n'+
        'Ꜿ\n'+
        'ꜿ\n'+
        'ꝯ con com cun cum\n'+
        'Ꝯ con com cun cum\n'+
        'ꝰ us os\n'+
        'ꝑ por per\n'+
        'ꝱ dum\n'+
        'Ꝫ et\n'+
        'ꝫ et\n'+
        'Ꝭ is\n'+
        'ꝭ is\n'+
        'ꝲ lum\n'+
        'ꝳ mum\n'+
        'ꝴ num\n'+
        'ꝵ rum\n'+
        'ꝶ rum\n'+
        'Ꝝ rum\n'+
        'ꝝ rum\n'+
        'ꝷ tum\n'+
        'ꝸ um\n'+
        'Ꝃ\n'+
        'ꝃ\n'+
        'Ꝁ\n'+
        'ꝁ\n'+
        'Ꝅ\n'+
        'ꝅ\n'+
        'Ꝉ\n'+
        'ꝉ\n'+
        'Ꝋ\n'+
        'ꝋ\n'+
        'Ꝓ pro por\n'+
        'ꝓ pro por\n'+
        'Ꝕ\n'+
        'ꝕ\n'+
        'Ꝑ par per\n'+
        'ꝑ par per\n'+
        'Ꝙ quod qui que\n'+
        'ꝙ quod qui que\n'+
        'q̄ que\n'+
        'qͥ qui\n'+
        'Ꝗ\n'+
        'ꝗ\n'+
        'ẜ\n'+
        'ẝ\n'+
        'Ꝟ\n'+
        'ꝟ\n'+
        'Ꝥ\n'+
        'ꝥ\n'+
        'Ꝧ\n'+
        'ꝧ\n'+
        't̉ tur\n'+
        'ꝫ m\n'+
        'U u v\n'+
        'u u v\n'+
        'V u v\n'+
        'v u v\n'+
        'J i\n'+
        'j i\n'
        );

var current_sigla_string = DEFAULT_SIGLA;

const PUNCTUATION = {
        '.': 1,
        ',': 1,
        '/': 1,
        '(': 1,
        ')': 1,
        ':': 1,
        ';': 1,
};

const WORD_ABBREVIATION = {
        'ꝙͣtum': 'quantum',
        'ſcd̓m': 'secundum',
        'ẜm': 'secundum',
        'ꝙͣ': 'quam',
        'ſcꝫ': 'scilicet',
        '.ſ.': 'scilicet',
	'qꝛ': 'quia',
	'itaqꝫ': 'itaque',
};

const MN = ['m', 'n'];

var sigla;

function get_sigla() {

        var lines = current_sigla_string.split('\n');

        sigla = {};

        lines.forEach(function(line) {
                var fields = line.split(' ');

                if (fields.length>0) {
                        sigla[fields[0]] = fields.slice(1);
                }
        });
}

function _expand_possible_variants(word) {

        if (word=='') {
            return [''];
        }

        var result = [];
        var next_bit = _expand_possible_variants(
                word.substr(1),
                );

        ch = word.substr(0, 1);

        if (ch in PLUS_NASAL) {
            var vowel = PLUS_NASAL[ch];

            MN.forEach(function(second_bit) {
                    next_bit.forEach(function(rest) {
                            result.push(vowel+second_bit+rest);
                    });
            });

        } else if (ch in sigla) {
                expansions = sigla[ch];
                if (expansions.length==0) {
                        console.log("Unknown siglum: "+ch);
                        // XXX this should be fiercer
                        return [''];
                }

                expansions.forEach(function(second_bit) {
                        next_bit.forEach(function(rest) {
                                result.push(second_bit+rest);
                        });
                });

        } else {
                next_bit.forEach(function(rest) {
                        result.push(ch+rest);
                });
        }

        return result;
}

function munge(word) {
        word = word.toLowerCase();

        if (word in WORD_ABBREVIATION) {
                return [WORD_ABBREVIATION[word]];
        }

        return _expand_possible_variants(word);
}


