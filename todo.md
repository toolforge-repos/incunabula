# js branch

- get hold of standard JS library to access mediawiki API
- make "open" dialogue which populates with list of pages
    (and updates the text)

-----------------

# general

- if the word isn't in the dictionary, we should clean up the ellipsis
- sigla file edit
- sigla entry shortcuts on the menu bar
- shortcut for long s and and
- deploy for the first time
- add ability to load a file off Wikisource
- show image in a pane
- add bookmark you can drag around
- ability to write back from a menu option
