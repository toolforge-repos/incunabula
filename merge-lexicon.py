import json

def main():
    with open('test/empty.json', 'r') as f:
        result = json.load(f)

    with open('lexicon.json', 'r') as f:
        lexicon = json.load(f)

    for word in lexicon:
        result.append({
            'model': 'latinist.lexeme',
            'fields': {
                'form': word,
                'exists': 'L',
                'expires': None,
                }
            })

    with open('lexicon-fixture.json', 'w') as f:
        json.dump(result, f)

if __name__=='__main__':
    main()
