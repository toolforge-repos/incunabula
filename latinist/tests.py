from django.test import TestCase, Client
from latinist.models import *
import pytest

EXPECTED_ANSWER = {
        'words': {
            '1': ['et'],
            '2': ['itaque'],
            '3': ['feles', 'catus'],
            '4': [],
            }
        }

class LatinistLookupTest(TestCase):
    fixtures = ['cats']

    def test_words_lookup(self):

        words = {
                '1': ['et'],
                '2': ['itaque'],
                '3': ['feles', 'catus', 'kitty'],
                '4': ['wombles'],
                }

        c = Client()

        response = c.post(
                '/api/lookup',
                {
                    'words': words,
                    },
                content_type = 'application/json',
                )

        self.assertEqual(response.json(),
                         EXPECTED_ANSWER,
                         )
