from django.apps import AppConfig


class LatinistConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'latinist'
