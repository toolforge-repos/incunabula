from django.shortcuts import render
from django.views import View
from django.http import Http404
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from latinist.models import Lexeme
import re
import latinist.mediawiki

ALL_DIGITS = re.compile(r'^[0-9]+$')
KNOWN = 'k'
UNKNOWN = 'u'

wikisource = latinist.mediawiki.Wikisource('la')

class Lookup(APIView):

    renderer_classes = [JSONRenderer]

    def post(self, request):

        incoming = request.data

        if isinstance(incoming, list):
            incoming = dict([
                (str(i), n)
                for i,n in enumerate(incoming)])
        elif isinstance(incoming, dict):
            pass
        else:
            raise Http404()

        queries = set([
                (block_label, word)
                 for block_label, word in incoming.items()
                 if ALL_DIGITS.fullmatch(block_label)
                 ])

        known = set([
                block_label
                for block_label, word in queries
                if self._is_known_word(word)
                ])

        unknown = set([
            block_label
            for block_label, word in queries
            ]) - known

        result = Response({
            KNOWN: sorted(known),
            UNKNOWN: sorted(unknown),
            })

        return result

    def _is_known_word(self, word):
        try:
            lexeme = Lexeme.objects.get(form=word)

            return lexeme.exists == lexeme.Exists.IS_LATIN

        except Lexeme.DoesNotExist:
            return False

class Pagelist(APIView):

    def get(self, request, workname):

        pagelist = wikisource.pagelist(workname)

        result = Response(pagelist)
        return result
