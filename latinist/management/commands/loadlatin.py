import xml.sax
import re
import sys
import json
from django.core.management.base import BaseCommand, CommandError
from latinist.models import Lexeme

class Command(BaseCommand):
    help = "Loads the database from a Wiktionary dump"

    def add_arguments(self, parser):
        parser.add_argument('enwikt', nargs=1, type=str,
                            help='a dump from English Wiktionary',
                            )
        parser.add_argument('--progress', '-p',
                            help='print progress messages',
                            action='store_true',
                            )

    def handle(self, *args, **options):

        enwikt = options['enwikt'][0]

        handler = LaHandler()

        if options['progress']:
            handler.show_progress_to = self.stdout

        with open(enwikt, 'r') as f:
            xml.sax.parse(f, handler=handler)

        self.stdout.write(
            self.style.SUCCESS(f'Done. Added {handler.added_count} entries.')
        )

class LaHandler(xml.sax.ContentHandler):
    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self._reading = None
        self._current_title = None
        self._current_text = None
        self.show_progress_to = None

        self.added_count = 0

    def startElement(self, name, attrs):
        if name in ['text', 'title']:
            assert self._reading is None

            self._reading = name

            if name=='text':
                self._current_text = ''
            elif name=='title':
                self._current_title = ''

    def characters(self, content):
        if self._reading is None:
            return
        elif self._reading=='title':
            self._current_title += content
        elif self._reading=='text':
            self._current_text += content

    def endElement(self, name):
        if self._reading is None:
            return
        elif self._reading!=name:
            return

        if self._reading=='text':
            self._handle_entry()

        self._reading = None

    def _handle_entry(self):

        if ':' in self._current_title:
            return

        if '==Latin' not in self._current_text:
            return

        newbie = Lexeme(
                form = self._current_title,
                exists = Lexeme.Exists.IS_LATIN,
                expires = None,
                )
        newbie.save()

        self.added_count += 1
        if self.added_count%100 == 0 and self.show_progress_to is not None:
            self.show_progress_to.write(
                    f'Processed {self.added_count} entries...'
            )


