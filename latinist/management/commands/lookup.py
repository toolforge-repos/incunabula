from django.core.management.base import BaseCommand, CommandError
from latinist.models import Lexeme

class Command(BaseCommand):
    help = "Looks up a word to see whether it's Latin"

    def add_arguments(self, parser):
        parser.add_argument('word', nargs=1, type=str)

    def handle(self, *args, **options):

        word = options['word'][0]

        lexeme = Lexeme.look_up(word)
        
        self.stdout.write(
            self.style.SUCCESS(f'{word}')
        )
