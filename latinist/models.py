import requests
from django.db import models
from django.utils.translation import gettext_lazy as _

class Lexeme(models.Model):

    form = models.CharField(
            max_length = 255,
            primary_key = True,
            )

    class Exists(models.TextChoices):
        IS_LATIN = 'L', _('is Latin')
        EXISTS_BUT_NOT_LATIN = 'N', _('exists, but not Latin')
        DOES_NOT_EXIST = 'X', _('does not exist')

    exists = models.CharField(
            max_length = 1,
            choices = Exists,
            )
    
    expires = models.DateField(
            default = None,
            null = True,
            )

    @classmethod
    def look_up(cls, word):

        url = f'https://en.wiktionary.org/wiki/{word}'

        headers = {
                'User-Agent': 'incunabula (https://incunabula.toolforge.org)'
        }

        response = requests.get(url, headers=headers)
        data = response.json()

