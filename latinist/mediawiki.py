import pywikibot
import pywikibot.proofreadpage
from django.http import Http404, HttpResponse

class Wikisource:

    def __init__(self, language):
        self.site = pywikibot.Site(language, 'wikisource')

    def pagelist(self, pagename):
        page = pywikibot.proofreadpage.IndexPage(
                self.site,
                f'Index:{pagename}')

        try:
            result = [
                    page.get_number(p)
                    for p in page.pages()]
        except pywikibot.exceptions.APIError as e:
            if e.code=='missingtitle':
                raise Http404
            else:
                raise HttpResponse(
                        status = 500,
                        message = f"I could not find the page list: {e.code}",
                        )

        return result
